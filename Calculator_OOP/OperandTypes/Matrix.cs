﻿using System;

namespace Calc_OOP.OperandTypes
{
    class Matrix : IOperand
    {
        private double[,] Mtrx = default;
        private int vDimensionMtrx = default;
        private int hDimensionMtrx = default;
        public void InitializeOperand(int v = 0, int h = 0)
        {
            if(v == 0 || h == 0)
            {
                Console.Write("Enter matrix horizontal dimension: ");
                while (!int.TryParse(Console.ReadLine(), out hDimensionMtrx))
                    Console.Write("Please, enter correct value: ");
                Console.Write("Enter matrix vertical dimension: ");
                while (!int.TryParse(Console.ReadLine(), out vDimensionMtrx))
                    Console.Write("Please, enter correct value: ");
            }
            else
            {
                hDimensionMtrx = h;
                vDimensionMtrx = v;
                Mtrx = new double[vDimensionMtrx, hDimensionMtrx];
            }

        }
        public void FillMatrix()
        {
            Console.WriteLine("Matrix filling...");
            Mtrx = new double[vDimensionMtrx, hDimensionMtrx];
            for (int i = 0; i < vDimensionMtrx; i++)
                for (int j = 0; j < hDimensionMtrx; j++)
                {
                    Console.Write($"Enter [{i + 1}, {j + 1}] matrix element: ");
                    while (!double.TryParse(Console.ReadLine(), out Mtrx[i, j]))
                        Console.Write("Please, enter correct value: ");
                }
        }
        public double this[int i, int j]
        {
            get { return Mtrx[i, j]; }
            set { Mtrx[i, j] = value; }
        }
        public int vDimension()
        {
            return vDimensionMtrx;
        }
        public int hDimension()
        {
            return hDimensionMtrx;
        }
    }
}
