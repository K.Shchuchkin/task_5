﻿using System;

namespace Calc_OOP.OperandTypes
{
    class Number : IOperand
    {
        private double operand = default;
        public void InitializeOperand(int v = 0, int h = 0)
        {
            Console.Write("Please, enter a number: ");
            while (!double.TryParse(Console.ReadLine(), out operand))
                Console.Write("Please, enter correct value: ");
        }
        public double Operand
        {
            get { return operand; }
            set { operand = value; }
        }
    }
}
