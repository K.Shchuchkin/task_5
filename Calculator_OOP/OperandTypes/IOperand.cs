﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calc_OOP.OperandTypes
{
    interface IOperand
    {
        public void InitializeOperand(int v = 0, int h = 0);
    }
}
