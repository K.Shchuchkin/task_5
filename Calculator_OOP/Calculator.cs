﻿using System;

namespace Calc_OOP
{
    class Calculator
    {
        public void Init()
        {
            OperandTypes.IOperand op1 = null, op2 = null;
            CalculatorTypes.ICalculator CL;
            while (true)
            {
                Console.WriteLine("Calculator initialized.\nChoose an operation:\n'm' - matrix calculator,\n'c' - regular calculator,\n'e' - exit,\n'r' - reset");
                char.TryParse(Console.ReadLine().ToLower(), out char operation);
                switch (operation)
                {
                    case 'm':
                        if (op1 == null || !(op1 is OperandTypes.Matrix))
                        {
                            op1 = new OperandTypes.Matrix();
                            op1.InitializeOperand();
                        }
                        else if (op1 != null) Console.WriteLine("Warning. The first operand is not null. You will start with the 2nd operand.");
                        op2 = new OperandTypes.Matrix();
                        op2.InitializeOperand();
                        CL = new CalculatorTypes.MatrixCalculator();
                        op1 = CL.Calculate(op1, op2);
                        break;
                    case 'c':
                        if (op1 == null || !(op1 is OperandTypes.Number))
                        {
                            op1 = new OperandTypes.Number();
                            op1.InitializeOperand();
                        }
                        else if (op1 != null) Console.WriteLine("Warning. The first operand is not null. You will start with the 2nd operand.");
                        op2 = new OperandTypes.Number();
                        op2.InitializeOperand();
                        CL = new CalculatorTypes.RegularCalculator();
                        op1 = CL.Calculate(op1, op2);
                        break;
                    case 'r':
                        op1 = null;
                        break;
                    case 'e':
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
