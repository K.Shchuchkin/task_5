﻿using System;

namespace Calc_OOP.CalculatorTypes
{
    class MatrixCalculator : ICalculator
    {
        private string ops = null;
        private char operation = default;
        public OperandTypes.IOperand Calculate(OperandTypes.IOperand op1, OperandTypes.IOperand op2)
        {
            ops = AvailableOperations(op1 as OperandTypes.Matrix, op2 as OperandTypes.Matrix); //Проверка на возможные операции
            switch (ops)
            {
                case "+-*ce":
                    Console.WriteLine("Choose an operation:\n'+' - addition,\n'-' - substraction,\n'*' - multiplication,\n'c' - clear,\n'e' - exit");
                    break;
                case "*ce":
                    Console.WriteLine("Choose an operation:\n'*' - multiplication,\n'c' - clear,\n'e' - exit");
                    break;
                case null:
                    Console.WriteLine("Sorry, no operation for these types of matrixes");
                    return op1;
            }
            Console.Write("Please, enter an operation: ");
            while(!char.TryParse(Console.ReadLine().ToLower(), out operation) || !ops.Contains(operation))
                Console.Write("Please, enter valid operation: ");
            switch (operation)
            {
                case 'c':
                    op1 = null;
                    return op1;
                case 'e':
                    return op1;
                default:
                    (op1 as OperandTypes.Matrix).FillMatrix();
                    break;
            }
            (op2 as OperandTypes.Matrix).FillMatrix();
            switch (operation)
            {
                case '+':
                    op1 = Add((OperandTypes.Matrix)op1, (OperandTypes.Matrix)op2);
                    break;
                case '-':
                    op1 = Subtract((OperandTypes.Matrix)op1, (OperandTypes.Matrix)op2);
                    break;
                case '*':
                    op1 = Multiplicate((OperandTypes.Matrix)op1, (OperandTypes.Matrix)op2);
                    break;
                default:
                    break;
            }
            return op1;
        }
        private OperandTypes.IOperand Add(OperandTypes.Matrix op1, OperandTypes.Matrix op2)
        {
            OperandTypes.Matrix ResMtrx = new OperandTypes.Matrix();
            ResMtrx.InitializeOperand(op1.vDimension(), op1.hDimension());
            for (int i = 0; i < ResMtrx.vDimension(); i++)
                for (int j = 0; j < ResMtrx.hDimension(); j++)
                    ResMtrx[i, j] = op1[i, j] + op2[i, j];
            for (int i = 0; i < ResMtrx.vDimension(); i++)
            {
                Console.Write("|");
                for (int j = 0; j < ResMtrx.hDimension(); j++)
                {
                    Console.Write(" {0, -4} ", ResMtrx[i, j]);
                }
                Console.WriteLine("|");
            }
            return ResMtrx;
        }
        private OperandTypes.IOperand Subtract(OperandTypes.Matrix op1, OperandTypes.Matrix op2)
        { 
            OperandTypes.Matrix ResMtrx = new OperandTypes.Matrix();
            ResMtrx.InitializeOperand(op1.vDimension(), op1.hDimension());
            for (int i = 0; i < ResMtrx.vDimension(); i++)
                for (int j = 0; j < ResMtrx.hDimension(); j++)
                    ResMtrx[i, j]= op1[i, j] - op2[i, j];
            for (int i = 0; i < ResMtrx.vDimension(); i++)
            {
                Console.Write("|");
                for (int j = 0; j < ResMtrx.hDimension(); j++)
                {
                    Console.Write(" {0, -4} ", ResMtrx[i, j]);
                }
                Console.WriteLine("|");
            }
            return ResMtrx;
        }
        private OperandTypes.IOperand Multiplicate(OperandTypes.Matrix op1, OperandTypes.Matrix op2)
        {
            OperandTypes.Matrix ResMtrx = new OperandTypes.Matrix();
            ResMtrx.InitializeOperand(op1.vDimension(), op2.hDimension());
            double temp = 0;
            for (int i = 0; i < ResMtrx.vDimension(); i++)
            {
                for (int j = 0; j < ResMtrx.hDimension(); j++)
                {
                    for (int c = 0; c < op2.vDimension(); c++)
                    {
                        temp += op1[i, c] * op2[c, j];
                    }
                    ResMtrx[i, j] = temp;
                    temp = 0;
                }
            }
            for (int i = 0; i < ResMtrx.vDimension(); i++)
            {
                Console.Write("|");
                for (int j = 0; j < ResMtrx.hDimension(); j++)
                {
                    Console.Write(" {0, -4} ", ResMtrx[i, j]);
                }
                Console.WriteLine("|");
            }
            return ResMtrx;
        }
        private string AvailableOperations(OperandTypes.Matrix op1, OperandTypes.Matrix op2)
        {
            if (op1.hDimension() == op2.hDimension() && op1.vDimension() == op2.vDimension())
                return "+-*ce";
            else if (op1.hDimension() == op2.vDimension())
                return "*ce";
            else
                return null;
        }
    }
}
