﻿namespace Calc_OOP.CalculatorTypes
{
    interface ICalculator
    {
        public OperandTypes.IOperand Calculate(OperandTypes.IOperand op1, OperandTypes.IOperand op2); //А - атомарность
    }
}
