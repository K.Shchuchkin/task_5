﻿using System;

namespace Calc_OOP.CalculatorTypes
{
    class RegularCalculator : ICalculator
    {
        private string ops = "+-/*ce";
        private char operation = default;
        public OperandTypes.IOperand Calculate(OperandTypes.IOperand op1, OperandTypes.IOperand op2)
        {
            Console.WriteLine("Please, choose an operation:");
            Console.WriteLine("Choose an operation:\n'+' - addition,\n'-' - substraction,\n'*' - multiplication,\n'/' - division,\n'c' - clear,\n'e' - exit");
            while (!char.TryParse(Console.ReadLine().ToLower(), out operation) || !ops.Contains(operation))
                Console.Write("Please, enter valid operation: ");
            if ((op2 as OperandTypes.Number).Operand == 0 && operation == '/')
            {
                Console.WriteLine("Sorry, cannot divide by 0");
                return op1;
            }
            switch (operation)
            {
                case '+':
                    op1 = Add((OperandTypes.Number)op1, (OperandTypes.Number)op2);
                    break;
                case '-':
                    op1 = Subtract((OperandTypes.Number)op1, (OperandTypes.Number)op2);
                    break;
                case '*':
                    op1 = Multiplicate((OperandTypes.Number)op1, (OperandTypes.Number)op2);
                    break;
                case '/':
                    op1 = Division((OperandTypes.Number)op1, (OperandTypes.Number)op2);
                    break;
                case 'c':
                    op1 = null;
                    break;
                case 'e':
                    return op1;
            }
            return op1;
        }
        private OperandTypes.IOperand Add(OperandTypes.Number op1, OperandTypes.Number op2)
        {
            OperandTypes.Number ResNumber = new OperandTypes.Number
            {
                Operand = op1.Operand + op2.Operand
            };
            Console.WriteLine($"{op1.Operand} + {op2.Operand} = {ResNumber.Operand}");
            return ResNumber;
        }
        private OperandTypes.IOperand Subtract(OperandTypes.Number op1, OperandTypes.Number op2)
        {
            OperandTypes.Number ResNumber = new OperandTypes.Number
            {
                Operand = op1.Operand - op2.Operand
            };
            Console.WriteLine($"{op1.Operand} - {op2.Operand} = {ResNumber.Operand}");
            return ResNumber;
        }
        private OperandTypes.IOperand Multiplicate(OperandTypes.Number op1, OperandTypes.Number op2)
        {
            OperandTypes.Number ResNumber = new OperandTypes.Number
            {
                Operand = op1.Operand * op2.Operand
            };
            Console.WriteLine($"{op1.Operand} * {op2.Operand} = {ResNumber.Operand}");
            return ResNumber;
        }
        private OperandTypes.IOperand Division(OperandTypes.Number op1, OperandTypes.Number op2)
        {
            OperandTypes.Number ResNumber = new OperandTypes.Number
            {
                Operand = Math.Round(op1.Operand / op2.Operand, 3)
            };
            Console.WriteLine($"{op1.Operand} / {op2.Operand} = {ResNumber.Operand}");
            return ResNumber;
        }
    }
}
